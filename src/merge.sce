/* merge.sce
* Provides the "merge" function who merges the given images.
* \author Moises TORRES AGUILAR
* \date 01/04/2019
*/
function  OUT = merge(IMG, HIMG, key)
  [w, h, rgb] = size(HIMG);
  rgb1 = size(IMG, 3);
  OUT = ones(w, h, max(rgb, rgb1));

  // If the hidden image is gray, hide it on every layer.
  if rgb <> rgb1 then
    if rgb == 1 then
      HIMG(:,:,2)=HIMG;
      HIMG(:,:,3)=HIMG(:,:,1);
    end
    
    // If the main image is gray, hide it on every layer.
    if rgb1 == 1 then
      IMG(:,:,2)=IMG;
      IMG(:,:,3)=IMG(:,:,1);
    end
  end

  OUT = uint8(bitor(bitand(IMG,256-2^(8-key)), bitand(ceil(HIMG/(2^key)), 2^key-1)));  
endfunction
