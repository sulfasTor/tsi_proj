/* extract.sce
* Provides the "extraction" function who extracts a image from the given image.
* \author Moises TORRES AGUILAR
* \date 01/04/2019
*/
function  OUT = extract(IMG, key)
  [w, h, rgb] = size(IMG);
  OUT = ones(w, h, rgb);
  OUT=uint8(IMG*(2^key));
endfunction
