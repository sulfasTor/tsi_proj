/* gui.sce
* Graphical User Interface
* \author Moises TORRES AGUILAR
* \date 01/04/2019
*/

funcprot(0)
f=figure('figure_position',[367,45],'figure_size',[936,783],'auto_resize','on','background',[-1],'figure_name','Steganography QR Watermark','dockable','off','infobar_visible','off','toolbar_visible','off','menubar_visible','off','default_axes','on','visible','off');

handles.dummy = 0;

handles.input_text=uicontrol(f,'unit','normalized','BackgroundColor',[-1,-1,-1],'Enable','on','FontAngle','normal','FontName','Cantarell','FontSize',[12],'FontUnits','points','FontWeight','normal','ForegroundColor',[-1,-1,-1],'HorizontalAlignment','left','ListboxTop',[],'Max',[1],'Min',[0],'Position',[0.0518053,0.0549199,0.5470958,0.1372998],'Relief','default','SliderStep',[0.01,0.1],'String','','Style','edit','Value',[0],'VerticalAlignment','middle','Visible','on','Tag','input_text','Callback','')

handles.img_viewer=uicontrol(f,'unit','normalized','BackgroundColor',[-1,-1,-1],'Enable','on','FontAngle','normal','FontName','Cantarell','FontSize',[12],'FontUnits','points','FontWeight','normal','ForegroundColor',[-1,-1,-1],'HorizontalAlignment','left','ListboxTop',[],'Max',[1],'Min',[0],'Position',[0.0502355,0.2997712,0.5510204,0.6533181],'Relief','default','SliderStep',[0.01,0.1],'String','welcome.png','Style','image','Value',[1,1,0,0,0],'VerticalAlignment','middle','Visible','on','Tag','img_viewer','Callback','img_viewer_callback(handles)')

handles.input_text_label=uicontrol(f,'unit','normalized','BackgroundColor',[0,0,0],'Enable','on','FontAngle','normal','FontName','Cantarell','FontSize',[15],'FontUnits','points','FontWeight','normal','ForegroundColor',[1,1,1],'HorizontalAlignment','left','ListboxTop',[],'Max',[1],'Min',[0],'Position',[0.0510204,0.1979405,0.1985871,0.0537757],'Relief','default','SliderStep',[0.01,0.1],'String','Hidden message','Style','text','Value',[0],'VerticalAlignment','middle','Visible','on','Tag','input_text_label','Callback','')

handles.but_gen=uicontrol(f,'unit','normalized','BackgroundColor',[-1,-1,-1],'Enable','on','FontAngle','normal','FontName','Cantarell','FontSize',[20],'FontUnits','points','FontWeight','normal','ForegroundColor',[-1,-1,-1],'HorizontalAlignment','center','ListboxTop',[],'Max',[1],'Min',[0],'Position',[0.700157,0.9038902,0.1962323,0.0411899],'Relief','default','SliderStep',[0.01,0.1],'String','Generate','Style','pushbutton','Value',[0],'VerticalAlignment','middle','Visible','on','Tag','but_gen','Callback','but_gen_callback(handles)')

handles.but_extract=uicontrol(f,'unit','normalized','BackgroundColor',[-1,-1,-1],'Enable','on','FontAngle','normal','FontName','Cantarell','FontSize',[20],'FontUnits','points','FontWeight','normal','ForegroundColor',[-1,-1,-1],'HorizontalAlignment','center','ListboxTop',[],'Max',[1],'Min',[0],'Position',[0.7040816,0.805492,0.1915228,0.0469108],'Relief','default','SliderStep',[0.01,0.1],'String','Extract','Style','pushbutton','Value',[0],'VerticalAlignment','middle','Visible','on','Tag','but_extract','Callback','but_extract_callback(handles)')

handles.but_load_second_image=uicontrol(f,'unit','normalized','BackgroundColor',[-1,-1,-1],'Enable','on','FontAngle','normal','FontName','Cantarell','FontSize',[18],'FontUnits','points','FontWeight','normal','ForegroundColor',[-1,-1,-1],'HorizontalAlignment','center','ListboxTop',[],'Max',[1],'Min',[0],'Position',[0.700157,0.4147493,0.1915228,0.0469108],'Relief','default','SliderStep',[0.01,0.1],'String','Load second image','Style','pushbutton','Value',[0],'VerticalAlignment','middle','Visible','on','Tag','but_load_second_image','Callback','but_load_second_image_callback(handles)')

handles.but_load=uicontrol(f,'unit','normalized','BackgroundColor',[-1,-1,-1],'Enable','on','FontAngle','normal','FontName','Cantarell','FontSize',[25],'FontUnits','points','FontWeight','normal','ForegroundColor',[-1,-1,-1],'HorizontalAlignment','center','ListboxTop',[],'Max',[1],'Min',[0],'Position',[0.700157,0.3147493,0.1916488,0.0761741],'Relief','default','SliderStep',[0.01,0.1],'String','Load image','Style','pushbutton','Value',[0],'VerticalAlignment','middle','Visible','on','Tag','but_load','Callback','but_load_callback(handles)')

handles.but_clear=uicontrol(f,'unit','normalized','BackgroundColor',[-1,-1,-1],'Enable','on','FontAngle','normal','FontName','Cantarell','FontSize',[20],'FontUnits','points','FontWeight','normal','ForegroundColor',[-1,-1,-1],'HorizontalAlignment','center','ListboxTop',[],'Max',[1],'Min',[0],'Position',[0.7034261,0.7044855,0.1948608,0.0435356],'Relief','default','SliderStep',[0.01,0.1],'String','Clear all','Style','pushbutton','Value',[0],'VerticalAlignment','middle','Visible','on','Tag','but_clear','Callback','but_clear_callback(handles)')

handles.but_qrgen=uicontrol(f,'unit','normalized','BackgroundColor',[-1,-1,-1],'Enable','on','FontAngle','normal','FontName','Cantarell','FontSize',[17],'FontUnits','points','FontWeight','normal','ForegroundColor',[-1,-1,-1],'HorizontalAlignment','center','ListboxTop',[],'Max',[1],'Min',[0],'Position',[0.4175589,0.1978892,0.1777302,0.0540897],'Relief','default','SliderStep',[0.01,0.1],'String','Generate QR Code','Style','pushbutton','Value',[0],'VerticalAlignment','middle','Visible','on','Tag','but_qrgen','Callback','but_qrgen_callback(handles)')

handles.status_label=uicontrol(f,'unit','normalized','BackgroundColor',[0,1,0],'Enable','on','FontAngle','normal','FontName','Courier','FontSize',[15],'FontUnits','points','FontWeight','normal','ForegroundColor',[-1,-1,-1],'HorizontalAlignment','left','ListboxTop',[],'Max',[1],'Min',[0],'Position',[0.003212,0.0065963,0.993576,0.0369393],'Relief','default','SliderStep',[0.01,0.1],'String','Done.','Style','text','Value',[0],'VerticalAlignment','middle','Visible','on','Tag','status_label','Callback','')

handles.second_image_label=uicontrol(f,'unit','normalized','BackgroundColor',[0,0,0],'Enable','on','FontAngle','normal','FontName','Cantarell','FontSize',[15],'FontUnits','points','FontWeight','normal','ForegroundColor',[0.70, 0.74, 0.80],'HorizontalAlignment','left','ListboxTop',[],'Max',[1],'Min',[0],'Position',[0.7141756,0.4647493,0.1915228,0.0469108],'Relief','default','SliderStep',[0.01,0.1],'String','Second image not loaded.','Style','text','Value',[0],'VerticalAlignment','middle','Visible','on','Tag','input_text_label','Callback','')

handles.lsb_slider=uicontrol(f,'unit','normalized','BackgroundColor',[-1,-1,-1],'Enable','on','FontAngle','normal','FontName','Dialog','FontSize',[10],'FontUnits','points','FontWeight','normal','ForegroundColor',[-1,-1,-1],'HorizontalAlignment','left','ListboxTop',[],'Max',[700],'Min',[100],'Position',[0.7034261,0.5544855,0.1948608,0.0435356],'Relief','default','SliderStep',[100 200],'String','','Style','slider','Value',[400],'VerticalAlignment','middle','Visible','on','Tag','lsb_slider','Callback','lsb_slider_callback(handles)')

handles.key_label=uicontrol(f,'unit','normalized','BackgroundColor',[0,0,0],'Enable','on','FontAngle','normal','FontName','Cantarell','FontSize',[20],'FontUnits','points','FontWeight','normal','ForegroundColor',[0.70, 0.74, 0.80],'HorizontalAlignment','left','ListboxTop',[],'Max',[1],'Min',[0],'Position',[0.7034261,0.6044855,0.1948608,0.0435356],'Relief','default','SliderStep',[1 2],'String','Key: 4','Style','text','Value',[0],'VerticalAlignment','middle','Visible','on','Tag','key_label','Callback','')

handles.name_label=uicontrol(f,'unit','normalized','BackgroundColor',[0,0,0],'Enable','on','FontAngle','normal','FontName','Cantarell','FontSize',[20],'FontUnits','points','FontWeight','normal','ForegroundColor',[1,1,1],'HorizontalAlignment','left','ListboxTop',[],'Max',[1],'Min',[0],'Position',[0.65,0.09,0.5,0.05],'Relief','default','SliderStep',[1 2],'String','Developped by Moises TORRES','Style','text','Value',[0],'VerticalAlignment','middle','Visible','on','Tag','name_label','Callback','')

handles.school_label=uicontrol(f,'unit','normalized','BackgroundColor',[0,0,0],'Enable','on','FontAngle','normal','FontName','Cantarell','FontSize',[20],'FontUnits','points','FontWeight','normal','ForegroundColor',[1, 1, 1],'HorizontalAlignment','left','ListboxTop',[],'Max',[1],'Min',[0],'Position',[0.6520343,0.045,0.394647,0.05],'Relief','default','SliderStep',[1 2],'String','PARIS8 L3 INFO [18902059]','Style','text','Value',[0],'VerticalAlignment','middle','Visible','on','Tag','school_label','Callback','')



f.visible = "on";

global IMG;
global IMG2;
global IMG3;
global QR;
global _KEY;

exec('merge.sce', -1);
exec('extract.sce', -1);

function set_status_bar_msg(handles, msg, status)
  set(handles.status_label,"String", msg);

  if status == "error"
    set(handles.status_label, "BackgroundColor", [1,0,0]);
  elseif status == "success"
    set(handles.status_label, "BackgroundColor", [0,1,0]);
  end

endfunction

function but_clear_callback(handles)
  global IMG;
  global IMG2;
  global IMG3;
  global QR;
  global _KEY;

  set(handles.img_viewer, 'String', 'welcome.png');
  set(handles.input_text, 'String', '');
  set(handles.second_image_label, 'String', 'Second image not loaded.');
  set(handles.key_label, 'String', "Key: 4");
  set(handles.lsb_slider, 'Value', [400]);
  set_status_bar_msg(handles, "Done.", "success");
  IMG=[];
  IMG2=[];
  IMG3=[];
  QR=[];
  _KEY = 4;
endfunction

function load_image(handles, img_path)
  global IMG;
  global IMG2;
  global IMG3;
  global QR;

  set(handles.input_text, 'String', '');
  set(handles.second_image_label, 'String', 'Second image not loaded.');
  set_status_bar_msg(handles, "Done", "success");
  IMG=[];
  IMG2=[];
  IMG3=[];
  QR=[];

  I = imread(img_path);
  [w, h, rgb] = size(I);
  if min(w, h) >= 21 then
    IMG = I;
    set_status_bar_msg(handles, "Loaded image.", "success");
  else
    set_status_bar_msg(handles, "Invalid image: length is less than 21 pixels.", "error");
  end
endfunction

function load_second_image(handles, img_path)
  global IMG2;
  global IMG;
  
  I = imread(img_path);
  [w, h, rgb] = size(I);
  [w1, h1, rgb1] = size(IMG);
  IMG2 = [];

  if min(w, h) < 21 then
    set_status_bar_msg(handles, "Invalid image: length is less than 21 pixels.", "error");
    return;
  end
  
  for i=1:rgb
    IMG2(:,:,i) = resize_matrix(I(:,:,i), w1, h1, 1);
  end

  if IMG2 <> [] then
    set_status_bar_msg(handles, "Loaded image.", "success");
    [path, fname, ext] = fileparts(img_path);
    set(handles.second_image_label, 'String', fname+ext);
  else
    set_status_bar_msg(handles, "Invalid image.", "error");
    set(handles.second_image_label, 'String', 'Second image not loaded.');
  end
endfunction

/* If the hidden image is smaller than the main image the difference is filled with ones. */
function padQRImage(img_path, w, h)
  I = imread(img_path);
  I = resize_matrix(I, w, h, 255);
  if imwrite(I, img_path) <> 1 then
    set_status_bar_msg(handles, "Failed to generate QR code.", "error");
  end
endfunction

function onload_callback(handles)
  global _KEY;

  _KEY = 4;
  img_path = get(handles.img_viewer, "String");
  if img_path <> "" then
    load_image(handles, img_path);
  end

endfunction

onload_callback(handles);

function img_viewer_callback(handles)
endfunction


function but_gen_callback(handles)
  global IMG;
  global QR;
  global IMG2;
  global IMG3;
  global _KEY;
  
  if IMG == [] then
    set_status_bar_msg(handles, "There is no first image.", "error");
    return;
  end

  input_text = get(handles.input_text ,"String");
  if IMG2 == [] && input_text == '' then
    set_status_bar_msg(handles, "There is no second image.", "error");
    return;
  end

  if IMG2 == [] then
    generate_qr_code(handles);
    IMG2 = bool2s(QR);
  end

  if ~isequal(size(IMG(:,:,1)), size(IMG2(:,:,1))) then
    set_status_bar_msg(handles, "Error while merging the images. Different sizes.", "error");
    return;
  end

  save_path = uiputfile(["*.png"], pwd(), "Save file...");

  if save_path == '' then
    return;
  end
  
  winId=progressionbar("Merging... ");
  try
    realtimeinit(0.3);
    IMG3=merge(IMG,IMG2,_KEY);
    for t=1:0.1:1
      realtime(3*t);
      progressionbar(winId);
    end
  catch
    set_status_bar_msg(handles, "Error while merging the images. " + string(lasterror()), "error");
    close(winId);
    return;
  end
  close(winId);
  
  if IMG3 == [] then
    set_status_bar_msg(handles, "Error while merging the images.", "error");
  else
    if fileparts(save_path, 'extension') == '' then
      save_path = save_path + ".png"
    end
    imwrite(IMG3, save_path);
    set_status_bar_msg(handles, "Merged images. Wrote to file " + save_path, "succes");
    scf();
    imshow(IMG3);
  end
endfunction


function but_extract_callback(handles)
  global IMG;
  global _KEY;

  if IMG==[] then
    set_status_bar_msg(handles, "There is no first image.", "error");
    return;
  end
  
  winId=progressionbar("Extracting... ");
  try
    realtimeinit(0.3);
    EXT=extract(IMG, _KEY);
    for t=1:0.1:1
      realtime(3*t);
      progressionbar(winId);
    end
  catch
    set_status_bar_msg(handles, "Error while extracting the image. " + string(lasterror()), "error");
    close(winId);
    return;
  end
  close(winId);

  if EXT == [] then
    set_status_bar_msg(handles, "Error while extracting the image. " + string(lasterror()), "error");
  else
    scf();
    imshow(EXT);
    set_status_bar_msg(handles, "Image extracted.", "succes");
  end

endfunction

function but_load_callback(handles)
  img_path = uigetfile(["*.bmp";"*.jpeg";"*.jpg";"*.png"], pwd(), 'Choose an image...');

  if img_path <> '' then
    set(handles.img_viewer, 'String', img_path);
    load_image(handles, img_path);
  end

endfunction

function but_qrgen_callback(handles)
  generate_qr_code(handles);
endfunction


function generate_qr_code(handles)
  global IMG;
  global QR;

  // Regex to avoid code injection
  input_text = strsubst(get(handles.input_text ,"String"), '/[""$&''()*;<>?[\]`{|}~]/', '', 'r');
  if input_text <> '' then
    status = host("qrencode");
    if (status <> 1)
      set_status_bar_msg(handles, "qrencode is not installed.", "error");
    else
      img_path = "qr.png";
      [w, c, rgb] = size(IMG);
      opts = ["qrencode", "-o", img_path, "-m", "0", string(input_text)]; 
      command = strcat(opts, " ");
      res = host(command);
      if res <> 0
	set_status_bar_msg(handles, "Failed to generate the qr.", "error");
      else
	padQRImage(img_path, w, c);
	QR = imread('qr.png');
	figure(3); imshow(QR);
	set_status_bar_msg(handles, "Generated QR.", "success");
      end
    end
  else
    set_status_bar_msg(handles, "There is no text.", "error");
  end
endfunction


function but_load_second_image_callback(handles)
  img_path = uigetfile(["*.bmp" ; "*.jpeg" ; "*.jpg" ; "*.png"], pwd(), 'Choose an image...');

  if img_path <> '' then
    load_second_image(handles, img_path)
  end
endfunction

function lsb_slider_callback(handles)
  global _KEY;

  _KEY = floor(get(handles.lsb_slider, "Value")/100);
  set(handles.key_label, "String", "Key: " + string(_KEY));
endfunction
